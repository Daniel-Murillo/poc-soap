const express = require('express');
const xml2js = require('xml2js');
const bodyParser = require("body-parser");
const soapRequest = require('easy-soap-request');
const REQUEST = require('./consts/request')

const app = express();

// Middleware para parsear request POST
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/** Servicios para parsear XML */
xmlService = new xml2js.Parser();
/** Servicio para construir XML */
xmlBuilder = new xml2js.Builder();

app.post('/', async function (req, res) {
    try {

        const param = req.body.zipCode || '75001';
        /** URL DE SERVICIO EXTERNO */

        const url = `https://graphical.weather.gov/xml/SOAP_server/ndfdXMLserver.php`;
        
        /** Headers de petición */
        const reqHeaders = {
            'Content-Type': 'text/xml;charset=UTF-8',
            'Content-Length': 'length',
            'soapAction': 'https://graphical.weather.gov/xml/DWMLgen/wsdl/ndfdXML.wsdl#LatLonListZipCode'
        };

        /** Objeto que será usado para construcción de XML de request */
        const xmlJson = REQUEST;

        // Se declara variable dentro del objeto para la petición
        xmlJson['soapenv:Envelope']['soapenv:Body'][0]['ndf:LatLonListZipCode'][0]['zipCodeList'][0]._ = param;

        /** XML con base a constante de objeto */
        const xmlParsed = xmlBuilder.buildObject(xmlJson);

        /** LLamada a servicio SOAP */
        const {
            response
        } = await soapRequest(url, reqHeaders, xmlParsed);
        const {
            body,
            statusCode
        } = response;

        /** Respuesta de llamada */
        let apiResponse;

        // Parsear Body de respuesta para una lectura en JSON
        await xmlService.parseString(body, (err, data) => {
            apiResponse =
                data['SOAP-ENV:Envelope']['SOAP-ENV:Body'][0]['ns1:LatLonListZipCodeResponse'][0]['listLatLonOut'][0]['_'];
        });

        // Parsear información XML dentro de objeto de respuesta
        await xmlService.parseString(apiResponse, (err, data) => {
            apiResponse = data['dwml']['latLonList'];
        });

        // Se manda petición parseada a un formato JSON
        res.status(statusCode).send(apiResponse);
    } catch (err) {
        res.status(500).send(err);
    }
});

app.listen(2345).on('listening', () => {
    console.log('App running on 2345');
});