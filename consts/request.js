
const REQUEST = {
	'soapenv:Envelope': {
		$: {
			'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
			'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema',
			'xmlns:soapenv': 'http://schemas.xmlsoap.org/soap/envelope/',
			'xmlns:ndf': 'https://graphical.weather.gov/xml/DWMLgen/wsdl/ndfdXML.wsdl'
		},
		'soapenv:Header': [''],
		'soapenv:Body': [
			{
				'ndf:LatLonListZipCode': [
					{
						'zipCodeList': [
							{
								/** VARIABLE A CAMBIAR */
								_: '70004',
								$: {
									'xsi:type': 'dwml:zipCodeListType',
									'xmlns:dwml': 'https://graphical.weather.gov/xml/DWMLgen/schema/DWML.xsd'
								}
							}
						]
					}
				]
			}
		]
	}
};

module.exports = REQUEST;